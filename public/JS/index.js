const bankBalanceElement = document.getElementById("bankBalance");
const loanElement = document.getElementById("loan");
const debtElement = document.getElementById("debt");
const loanBtnElement = document.getElementById("loanBtn");
const loanErrorElement = document.getElementById("loanError");
const bankBtnElement = document.getElementById("bankBtn");
const workBtnElement = document.getElementById("workBtn");
const bankPayElement = document.getElementById("bankPay");
const repayLoanBtnElement = document.getElementById("repayLoanBtn");
const computersElement = document.getElementById("laptopType");
const priceElement = document.getElementById("price");
const descriptionElement = document.getElementById("displayDescription");
const computerImgElement = document.getElementById("computerImg");
const specsListElement = document.getElementById("specsList");
const purchaseBtnElement = document.getElementById("purchaseBtn");

const currencyFormat = new Intl.NumberFormat('no-NO', {style: 'currency', currency: 'nok'})

let totalBankBalance = 0;
let loan = 0;

// Get Loan button section
loanBtnElement.addEventListener("click", function () {
    loanErrorElement.innerText = "";
    if (loanExists(loan)) {
        loanErrorElement.innerText = "Loan already exists!"
        return;
    }
    
    let loanInput = window.prompt("Type the amount: ");
    if (loanInput > totalBankBalance * 2) {
        loanErrorElement.innerText = "The amount is too high!"
        return;
    }
    loan = loanInput;
    debt(loan);
});

// Checks if loan exists
function loanExists(input) {
    if (input > 0)
    return true;
    return false;
}

// Debt functionality section
function debt(input) {
    debtElement.hidden = !(input > 0);
    loanElement.hidden = !(input > 0);
    repayLoanBtnElement.hidden = !(input >0);
    loanElement.innerText = currencyFormat.format(input);
}

// Repay loan functionality section
function repayLoan(input){
    if(input > loan){
        let returnLoan = input - loan;
        loan = 0;
        return returnLoan;
    }
    loan -= input;
    return 0;
}

let currentPay = 0;

// Bank button section
bankBtnElement.addEventListener("click", function() {
    if(loanExists(loan)){
        let deduction = currentPay * 0.1;
        currentPay -= deduction;
        let remainderAfterPay = repayLoan(deduction);
        currentPay += remainderAfterPay;
        debt(loan);
    }
    
    totalBankBalance += currentPay;
    currentPay = 0;
    bankPayElement.innerText = currencyFormat.format(currentPay);
    bankBalanceElement.innerText = currencyFormat.format(totalBankBalance);
})

// Work button section
workBtnElement.addEventListener("click", function() {
    currentPay += 100;
    bankPayElement.innerText = currencyFormat.format(currentPay);
})

// Repay Loan button section
repayLoanBtnElement.addEventListener("click", function() {
    totalBankBalance += repayLoan(currentPay);
    currentPay = 0;
    debt(loan);
    bankPayElement.innerText = currencyFormat.format(currentPay);
    bankBalanceElement.innerText = currencyFormat.format(totalBankBalance);
})

// Api fetch and computer description and features section
let computers = [];

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToList(computers));

const addComputersToList = (computers) => {
    computers.forEach(x => addComputerToList(x));
    priceElement.innerText = computers[0].price;
    descriptionElement.innerText = computers[0].description;
    specsListElement.innerText = computers[0].specs;
}

const addComputerToList = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(document.createTextNode(computer.description));
    computersElement.appendChild(computerElement);
}

// Could not finish this in time, but I have tried something.
const baseApi = "https://noroff-komputer-store-api.herokuapp.com/";

const imgUrl = baseApi + computers.image;

const getComputers = (p) => {
    const selectedComputer = computers[p.target.selectedIndex];
    priceElement.innerText = selectedComputer.price;
    descriptionElement.innerText = selectedComputer.description;
    specsListElement.innerText = selectedComputer.specs;
    computerImgElement.innerText = imgUrl;
}

computersElement.addEventListener("change", getComputers);

// Purchase button section
purchaseBtnElement.addEventListener("click", function() {
    let price = priceElement.innerText.split(" ")[0];
    
    if (totalBankBalance >= price){

        totalBankBalance -= price;

        alert("You have purchased a new Komputer! Good luck with that!😋");

        bankBalanceElement.innerText = currencyFormat.format(totalBankBalance);

    } else {
        alert("You need more Money 💳!");
    }
})